/*
 * @Author: Hang
 * @Date: 2020-07-14 16:35:44
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 00:13:38
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/node/router/routes/upload.js
 * @Description: 暂无描述
 */

const sql = require('../../sql');
const fs = require('fs');
const { getParams, check, uploader } = require('../../common');

// 上传单个文件
let timer = null;
const uploadOne = (req, res) => {
  const params = getParams(req);
  // 每5秒的第一次上传到第10片时会失败
  if (params.chunk === '10' && !timer) {
    timer = setTimeout(() => {
      timer = null;
    }, 5000);
    res.send({
      code: '-1',
      msg: '上传失败'
    });
  } else {
    res.send({
      code: '0',
      msg: '上传成功'
    });
  }
};

module.exports = {
  // 单个文件
  '/upload': uploader({ fn: uploadOne })
};
