/*
 * @Author: Hang
 * @Date: 2020-07-13 13:12:50
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 18:36:41
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/node/router/routes/index.js
 * @Description: 暂无描述
 */

const table = require('./table');
const upload = require('./upload');
const fileManage = require('./fileManage');
const modal = require('./modal');

const routes = {
  ...table,
  ...upload,
  ...fileManage,
  ...modal
};

module.exports = routes;
