/*
 * @Author: Hang
 * @Date: 2020-07-13 13:20:33
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 18:37:27
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/node/router/routes/modal.js
 * @Description: 表格接口
 */

const sql = require('../../sql');
const fs = require('fs');
const { getParams, check } = require('../../common');

const operate = (req, res) => {
  setTimeout(() => {
    res.send({
      code: '0',
      msg: '提交成功'
    });
  }, 500);
};

module.exports = {
  '/operate': operate
};
