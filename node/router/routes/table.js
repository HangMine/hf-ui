/*
 * @Author: Hang
 * @Date: 2020-07-13 13:20:33
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 12:18:06
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/node/router/routes/table.js
 * @Description: 表格接口
 */

const sql = require('../../sql');
const fs = require('fs');
const path = require('path');
const { getParams, check } = require('../../common');
const tableData = require('../../testData/table');

const getSlicesRows = (rows, page, size) => {
  const start = (page - 1) * size;
  const end = page * size;
  return rows.slice(start, end);
};

const sort_date = (currentRows, key, order) => {
  currentRows.sort((a, b) => {
    let v1 = getDate(a, key);
    let v2 = getDate(b, key);
    return order == 'ascending' ? v1 - v2 : v2 - v1;
  });
};

const getDate = (val, key) => {
  val = (val[key] && val[key].name) || val[key];
  if (isNaN(val)) {
    if (val == '--') return new Date(0); //兼容'--'的情况
    val = val.replace(/-/g, '/');
    return new Date(val);
  } else {
    return +val;
  }
};

const getTable = (req, res) => {
  const { page, size, action, sortKey, sortOrder } = getParams(req);
  // 是导出
  if (action === 'export') {
    res.download(path.resolve(__dirname, '../../testData/test.txt'), 'test.txt', err => {
      if (err) {
        console.log(err);
        res.send({
          code: '-1',
          msg: '导出失败'
        });
      }
    });
    return;
  }
  let rows = [...tableData.rows];
  // 需要排序
  if (sortOrder && sortKey === 'date') {
    sort_date(rows, sortKey, sortOrder);
  }
  // 获取切割后的数据
  let data = {};
  if (!page || !size) {
    data = { ...tableData, rows };
  } else {
    data = { ...tableData, rows: getSlicesRows(rows, page, size) };
  }
  setTimeout(() => {
    res.send({
      code: '0',
      data,
      msg: '获取表格数据成功'
    });
  }, 500);
};

module.exports = {
  '/getTable': getTable
};
