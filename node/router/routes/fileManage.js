/*
 * @Author: Hang
 * @Date: 2020-07-13 13:20:33
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 01:27:11
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/node/router/routes/fileManage.js
 * @Description: 表格接口
 */

const sql = require('../../sql');
const fs = require('fs');
const path = require('path');
const { getParams, check, uploader } = require('../../common');
const files = require('../../testData/fileManage');

const list = (req, res) => {
  setTimeout(() => {
    res.send({
      code: '0',
      data: files.list,
      msg: '获取文件列表成功'
    });
  }, 500);
};

const search = (req, res) => {
  setTimeout(() => {
    res.send({
      code: '0',
      data: files.search,
      msg: '获取搜索列表成功'
    });
  }, 500);
};

const upload = (req, res) => {
  const { chunk, chunks, fileName } = getParams(req);
  if (+chunk === +chunks - 1) {
    // 上传完成
    files.list.push({
      name: fileName,
      type: fileName.split('.').reverse()[0],
      size: '666KB',
      url: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3902194959,1462735767&fm=26&gp=0.jpg',
      date: '2020/06/06'
    });
  }

  setTimeout(() => {
    res.send({
      code: '0',
      msg: '上传文件成功'
    });
  }, 500);
};

const mkDir = (req, res) => {
  const { name, path } = getParams(req);

  files.list.push({
    name,
    type: 'dir',
    date: '2020/06/06'
  });

  setTimeout(() => {
    res.send({
      code: '0',
      msg: '新建文件夹成功'
    });
  }, 500);
};

const rename = (req, res) => {
  const { name, new_name, path } = getParams(req);

  const file = files.list.find(item => item.name === name);

  file.name = new_name;

  setTimeout(() => {
    res.send({
      code: '0',
      msg: '修改名字成功'
    });
  }, 500);
};

const _delete = (req, res) => {
  const { name = [], path } = getParams(req);

  const list = files.list;

  for (const file of list) {
    if (name.includes(file.name)) {
      const index = list.findIndex(item => item.name === file.name);
      list.splice(index, 1);
    }
  }

  setTimeout(() => {
    res.send({
      code: '0',
      msg: '删除成功'
    });
  }, 500);
};

const down = (req, res) => {
  // const params = getParams(req);
  res.download(path.resolve(__dirname, 'fileManage.js'), 'fileManage.js', err => {
    if (err) {
      console.log(err);
      res.send({
        code: '-1',
        msg: '下载失败'
      });
    }
  });
};

module.exports = {
  '/fileManage/list': list,
  '/fileManage/search': search,
  '/fileManage/upload': uploader({ fn: upload }),
  '/fileManage/mkDir': mkDir,
  '/fileManage/rename': rename,
  '/fileManage/delete': _delete,
  '/fileManage/down': down
};
