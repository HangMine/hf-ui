/*
 * @Author: Hang
 * @Date: 2020-07-14 23:14:36
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-14 23:45:45
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/node/testData/fileManage.js
 * @Description: 暂无描述
 */
module.exports = {
  list: [
    {
      name: '文件夹1',
      type: 'dir',
      size: '123KB',
      url: '',
      date: '2020/06/06'
    },
    {
      name: '文本1',
      type: 'txt',
      size: '12KB',
      url: '',
      date: '2020/06/06'
    },
    {
      name: '图片1',
      type: 'jpg',
      size: '66KB',
      url: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3902194959,1462735767&fm=26&gp=0.jpg',
      date: '2020/06/06'
    },
    {
      name: '其它文件1',
      type: '',
      size: '666KB',
      url: '',
      date: '2020/06/06'
    }
  ],
  search: [
    {
      file: '文件夹2',
      type: 'dir',
      path: '/dir2',
      url: ''
    },
    {
      file: '图片2',
      type: 'png',
      path: '/img2',
      url:
        'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1594751479552&di=939c9bbcd5a78a0d8a3a7b9a42e31159&imgtype=0&src=http%3A%2F%2Ft9.baidu.com%2Fit%2Fu%3D86853839%2C3576305254%26fm%3D79%26app%3D86%26f%3DJPEG%3Fw%3D750%26h%3D390'
    }
  ]
};
