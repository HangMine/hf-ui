/*
 * @Author: Hang
 * @Date: 2020-07-11 15:15:17
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 18:55:16
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/.eslintrc.js
 * @Description: 暂无描述
 */

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', 'eslint:recommended'],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': 'warn',
    'vue/no-unused-vars': 'warn',
    'no-control-regex': 'off',
    'no-prototype-builtins': 'off'
  }
};
