/*
 * @Author: Hang
 * @Date: 2020-07-11 22:43:27
 * @LastEditors: Hang
 * @LastEditTime: 2020-08-23 13:47:27
 * @FilePath: /demon-home/Users/zhengmukang/Desktop/hf-ui/vue.config copy.js
 * @Description: vue-config备份
 */
const { spawn, exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const join = path.join; //拼接路径
function resolve(dir) {
  return path.resolve(__dirname, dir);
}

// 获取packacges里的组件
function getEntries(path) {
  let files = fs.readdirSync(resolve(path));
  const entries = files.reduce((ret, item) => {
    const itemPath = join(path, item);
    const isDir = fs.statSync(itemPath).isDirectory();
    if (isDir) {
      ret[item] = resolve(join(itemPath, 'index'));
    } else {
      const [name] = item.split('.');
      ret[name] = resolve(`${itemPath}`);
    }
    return ret;
  }, {});
  return entries;
}

const alias = {
  '@': resolve('/'),
  '@examples': resolve('examples'),
  '@packages': resolve('packages'),
  '@utils': resolve('utils'),
  '@assets': resolve('assets'),
  '@api': resolve('api'),
};

//开发环境配置
const devConfig = {
  pages: {
    index: {
      entry: 'examples/main.js',
      template: 'public/index.html',
      filename: 'index.html',
    },
  },
  configureWebpack: {
    devtool: 'source-map',
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias,
    },
  },
  devServer: {
    proxy: {
      '/local': {
        target: 'http://localhost:8888',
        pathRewrite: {
          '^/local': '',
        },
        changeOrigin: true,
        secure: false,
        // logLevel: 'debug'
      },
    },
  },
};
const buildConfig = {
  outputDir: 'lib',
  productionSourceMap: false,
  configureWebpack: {
    entry: {
      ...getEntries('packages'),
    },
    output: {
      filename: '[name]/index.js',
      libraryTarget: 'umd',
    },
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias,
    },
  },
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks');
    config.plugins.delete('copy');
    config.plugins.delete('html');
    config.plugins.delete('preload');
    config.plugins.delete('prefetch');
    config.plugins.delete('hmr');
    config.entryPoints.delete('app');

    config.module
      .rule('fonts')
      .use('url-loader')
      .tap((option) => {
        option.fallback.options.name = 'static/fonts/[name].[hash:8].[ext]';
        return option;
      });
  },
  css: {
    sourceMap: true,
    extract: {
      filename: '[name]/index.css', //在lib文件夹中建立style文件夹中，生成对应的css文件。
    },
  },
};
const isDev = process.env.NODE_ENV === 'development';

// 开发环境下，开启本地服务
if (isDev) {
  spawn('node', ['node/index'], { stdio: 'inherit' });
}
module.exports = isDev ? devConfig : buildConfig;

// module.exports = {
//   // 此插件需要css分离
//   css: {
//     // 是否使用css分离插件 ExtractTextPlugin
//     // extract: false,
//     extract: {
//       filename: "style/[name].css", //在lib文件夹中建立style文件夹中，生成对应的css文件。
//     },
//     // 开启 CSS source maps?
//     sourceMap: true,
//     // css预设器配置项
//     loaderOptions: {},
//     // 启用 CSS modules for all css / pre-processor files.
//     modules: false,
//   },
// };
