/*
 * @Author: Hang
 * @Date: 2020-04-27 10:04:00
 * @LastEditors: Hang
 * @LastEditTime: 2020-08-23 14:16:52
 * @FilePath: /demon-home/Users/zhengmukang/Desktop/hf-ui/examples/router/index.js
 * @Description: 暂无描述
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '../views/index.vue';
import EmptyRouterView from '../layouts/EmptyRouterView';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    icon: 'el-icon-finished',
    component: Index,
    redirect: '/components/table',
    children: [
      {
        path: 'components',
        name: 'components',
        title: '组件',
        component: EmptyRouterView,
        children: [
          {
            path: 'install',
            name: 'install',
            title: '安装',

            icon: 'el-icon-files',
            componentType: 'DevGuide',
            component: () => import('../views/install')
          },
          {
            path: 'quickStart',
            name: 'quickStart',
            title: '快速上手',
            icon: 'el-icon-files',
            componentType: 'DevGuide',
            component: () => import('../views/quickStart')
          },
          {
            path: 'table',
            name: 'table',
            title: '集成表格',
            icon: 'el-icon-files',
            componentType: 'Basic',
            component: () => import('../views/table')
          },
          {
            path: 'form',
            name: 'form',
            title: '动态表单',
            icon: 'el-icon-tickets',
            componentType: 'Basic',
            component: () => import('../views/form')
          },
          {
            path: 'cascader',
            name: 'cascader',
            title: '多级级联',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/cascader')
          },
          {
            path: 'timeRange',
            name: 'timeRange',
            title: '时间段选取',
            icon: 'el-icon-date',
            componentType: 'Basic',
            component: () => import('../views/timeRange')
          },
          {
            path: 'checkbox',
            name: 'checkbox',
            title: '可视窗口的大量勾选框',
            icon: 'el-icon-guide',
            componentType: 'Basic',
            component: () => import('../views/checkbox')
          },
          {
            path: 'upload',
            name: 'upload',
            title: '批量切片上传',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/upload')
          },
          {
            path: 'fileManage',
            name: 'fileManage',
            title: '文件管理',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/fileManage')
          },
          {
            path: 'scroll',
            name: 'scroll',
            title: '滚动',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/scroll')
          },
          {
            path: 'hideScroll',
            name: 'hideScroll',
            title: '隐藏滚动条',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/hideScroll')
          },
          {
            path: 'overHide',
            name: 'overHide',
            title: '高度超出隐藏',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/overHide')
          },
          {
            path: 'export',
            name: 'export',
            title: '导出',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/export')
          },
          {
            path: 'modal',
            name: 'modal',
            title: '模态框',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/modal')
          },
          {
            path: 'fix',
            name: 'fix',
            title: '固钉',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/fix')
          },
          {
            path: 'typeInput',
            name: 'typeInput',
            title: '类型输入框',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/typeInput')
          },
          {
            path: 'contents',
            name: 'contents',
            title: '目录',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/contents')
          },
          {
            path: 'hightlight',
            name: 'hightlight',
            title: '代码高亮',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/hightlight')
          },
          {
            path: 'test',
            name: 'test',
            title: '测试',
            icon: 'el-icon-magic-stick',
            componentType: 'Basic',
            component: () => import('../views/test')
          }
        ]
      }
    ]
  }
];

const router = new VueRouter({
  routes
});

export default router;
