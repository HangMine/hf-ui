/*
 * @Author: Hang
 * @Date: 2020-07-11 15:15:17
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 15:25:27
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/main.js
 * @Description: 暂无描述
 */

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import hfUI from '@packages';
import '@api/http';
import '@examples/assets/css/index.scss';

import examplesComponents from './components';

import client from '@utils/client';

if (client.system.win) {
  // 如果是windows系统，增加滚动条样式
  document.documentElement.classList.add('windows');
}

Vue.use(ElementUI);
Vue.use(hfUI);
Vue.use(examplesComponents);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
