/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-17 10:49:51
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/table/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'FileManage Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 100
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 200
      }
    ],
    data: [
      {
        params: 'urls',
        explain: '操作URL',
        type: 'Object',
        validate: '-',
        default: '-'
      },
      {
        params: 'accept',
        explain: '上传文件类型限制',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'fileType',
        explain: '显示文件类型限制',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'files',
        explain: '暴露当前选中的文件列表',
        type: 'Array',
        validate: '-',
        default: 'false'
      },
      {
        params: 'file',
        explain: '暴露当前选中的文件',
        type: 'Object',
        validate: '-',
        default: '-'
      }
    ]
  },
  {
    title: 'FileManage Methods',
    columns: [
      {
        key: 'funtionName',
        title: '方法名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      }
    ],
    data: [
      {
        funtionName: 'clearChecked',
        explain: '清空选中',
        params: '-'
      },
      {
        funtionName: 'setAllChecked',
        explain: '选中所有数据',
        params: '-'
      }
    ]
  }
];
