/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 16:34:38
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/hightlight/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'HightLight Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 120
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'code',
        explain: '需要高亮的代码',
        type: 'Any',
        validate: '-',
        default: '-'
      }
    ]
  }
];
