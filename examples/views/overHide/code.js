/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 11:03:17
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/overHide/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
    <h-over-hide v-model="show" :height="120">
      <p>test</p>
      <p>test</p>
      <p>test</p>
      <p>超出的test</p>
      <p>超出的test</p>
      <p>超出的test</p>
    </h-over-hide>
</template>

<script>
export default {
  data() {
    return {
      show: false
    };
  }
};
</script>
 `;

export default {
  test1
};
