/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 21:33:31
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/overHide/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'OverHide Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'height',
        explain: '高度',
        type: 'Number, String',
        validate: '-',
        default: '100'
      },
      {
        params: 'maxHeight',
        explain: '最大高度',
        type: 'Number, String',
        validate: '-',
        default: '-'
      },
      {
        params: 'value',
        explain: '状态：是否展开',
        type: 'Boolean',
        validate: '-',
        default: 'false'
      },
      {
        params: 'hideText',
        explain: '展开按钮文字描述',
        type: 'String',
        validate: '-',
        default: '展开'
      },
      {
        params: 'showText',
        explain: '收起按钮文字描述',
        type: 'String',
        validate: '-',
        default: '收起'
      },
      {
        params: 'textAlign',
        explain: '按钮位置，可选值为text-align属性值',
        type: 'String',
        validate: '-',
        default: 'center'
      }
    ]
  },
  {
    title: 'OverHide Events',
    columns: [
      {
        key: 'eventName',
        title: '事件名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '回调参数',
        minWidth: 200
      }
    ],
    data: [
      {
        eventName: 'input',
        explain: '是否展开',
        params: '展开为true，否则false'
      }
    ]
  },
  {
    title: 'Overhide Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: 'default',
        explain: '需要超出隐藏的元素'
      }
    ]
  }
];
