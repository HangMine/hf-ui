/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 11:44:28
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/form/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
<h-form :data="formData" v-model="params" :cols="3"> </h-form>
</template>

<script>
export default {
  data() {
    return {
      formData: [
        {
          key: "test1",
          title: "测试1",
        },
        {
          key: "test2",
          title: "测试2",
          type: "select",
          options: [
            {
              key: "1",
              title: "选项一",
            },
            {
              key: "2",
              title: "选项二",
            },
          ],
        },
        {
          key: "test3",
          title: "测试3",
          type: "date",
        },
        {
          key: "test4",
          title: "测试4",
          type: "checkbox",
          options: [
            {
              key: "1",
              title: "选项一",
            },
            {
              key: "2",
              title: "选项二",
            },
          ],
        },
        {
          key: "test5",
          title: "测试5",
          type: "radio",
          options: [
            {
              key: "1",
              title: "选项一",
            },
            {
              key: "2",
              title: "选项二",
            },
          ],
        },
        {
          key: "test6",
          title: "测试6",
          type: "date",
          type2: "daterange",
        },
      ],
      params: {},
    };
  }
};
</script>
 `;

const test2 = `
<template>
  <h-form :data="formData" v-model="parmas" inline> </h-form>
</template>

<script>
export default {
  data() {
    return {
      formData: [
        {
          key: "test1",
          title: "测试1",
        },
        {
          key: "test2",
          title: "测试2",
          type: "select",
          options: [
            {
              key: "1",
              title: "选项一",
            },
            {
              key: "2",
              title: "选项二",
            },
          ],
        },
        {
          key: "test3",
          title: "测试3",
          type: "date",
        },
        {
          key: "test4",
          title: "测试4",
          type: "date",
          type2: "daterange",
        },
      ],
      parmas: {},
    };
  }
};
</script>
 `;

export default {
  test1,
  test2
};
