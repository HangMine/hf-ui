/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 21:28:39
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/form/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Form Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'data',
        explain: '表单配置数据',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'value',
        explain: '表单数据对象',
        type: 'Object',
        validate: '-',
        default: '-'
      },
      {
        params: 'cols',
        explain: '列数',
        type: 'Number, String',
        validate: '-',
        default: '2'
      },
      {
        params: 'gutter',
        explain: '间距',
        type: 'Number',
        validate: '-',
        default: '-'
      },
      {
        params: 'disabled',
        explain: '是否可用',
        type: 'Boolean',
        validate: '-',
        default: 'false'
      }
    ]
  },
  {
    title: 'Form Events',
    columns: [
      {
        key: 'eventName',
        title: '事件名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '回调参数',
        minWidth: 200
      }
    ],
    data: [
      {
        eventName: 'input',
        explain: '表单数据对象修改',
        params: '当前表单数据对象'
      }
    ]
  },
  {
    title: 'Form Methods',
    columns: [
      {
        key: 'funtionName',
        title: '方法名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      }
    ],
    data: [
      {
        funtionName: 'formCheck',
        explain: '表单验证',
        params: '-'
      },
      {
        funtionName: 'clearCheck',
        explain: '表单清除校验',
        params: '-'
      }
    ]
  },
  {
    title: 'Table Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: '表单key值',
        explain: '自定义表单内容'
      },
      {
        name: 'default',
        explain: '表单后面'
      }
    ]
  }
];
