/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 19:09:04
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/modal/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
      <el-button type="primary" @click="show = true">模态框</el-button>
      <h-modal
        v-model="show"
        :form="form"
        :params.sync="formParams"
        :handleParams="handleParams"
        submit-url="/local/operate"
      ></h-modal>
</template>

<script>
export default {
  data() {
    return {
      show: false,
      form: [
        {
          key: 'test1',
          title: '测试1',
          rules: [{ required: true }]
        },
        {
          key: 'test2',
          title: '测试2',
          type: 'select',
          options: [
            {
              key: '1',
              title: '选项一'
            },
            {
              key: '2',
              title: '选项二'
            }
          ]
        },
        {
          key: 'test3',
          title: '测试3',
          type: 'date'
        },
        {
          key: 'test4',
          title: '测试4',
          type: 'checkbox',
          options: [
            {
              key: '1',
              title: '选项一'
            },
            {
              key: '2',
              title: '选项二'
            }
          ]
        },
        {
          key: 'test5',
          title: '测试5',
          type: 'radio',
          options: [
            {
              key: '1',
              title: '选项一'
            },
            {
              key: '2',
              title: '选项二'
            }
          ]
        },
        {
          key: 'test6',
          title: '测试6',
          type: 'date',
          type2: 'daterange'
        }
      ],
      formParams: {}
    };
  },
  methods: {
    handleParams(params) {
      params.test1 = 1;
    }
  }
};
</script>
 `;

export default {
  test1
};
