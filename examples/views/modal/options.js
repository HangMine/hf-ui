/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-17 10:49:51
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/table/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Modal Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'value',
        explain: '是否显示',
        type: 'Boolean',
        validate: '-',
        default: 'false'
      },
      {
        params: 'form',
        explain: '表单配置数据',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'params',
        explain: '表单数据对象',
        type: 'Object',
        validate: '-',
        default: '-'
      },
      {
        params: 'handleParams',
        explain: '处理表单数据对象函数',
        type: 'params=>{...处理内容,return newParams}',
        validate: '-',
        default: '-'
      },
      {
        params: 'submit',
        explain: '提交，待开发',
        type: 'function',
        validate: '-',
        default: '-'
      },
      {
        params: 'submitUrl',
        explain: '提交调用Url，待开发',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'formProps',
        explain: '表单配置参数',
        type: 'Object',
        validate: '-',
        default: "{ size: 'small' }"
      },
      {
        params: 'title',
        explain: '模态框标题',
        type: 'String',
        validate: '-',
        default: "模态框"
      },
      {
        params: 'formValues',
        explain: '赋值的表单数据',
        type: 'Object',
        validate: '-',
        default: '-'
      }
    ]
  }
];
