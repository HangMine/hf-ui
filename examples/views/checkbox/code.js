/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 19:48:41
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/checkbox/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
  <h-checkbox-group
    v-model="checkedList"
    :data="checkboxData"
    :cols="4"
    height="40vh"
    placeholder="关键字，请输入..."
  >
  </h-checkbox-group>
</template>

<script>
import code from "./code";
export default {
  name: "d-checkbox",
  data() {
    const checkboxData = Array(1000)
      .fill()
      .map((item, i) => ({
        id: i,
        name: \`勾选框\${i}\`,
      }));
    return {
      code,
      checkboxData,
      checkedList: [],
    };
  },
  created() {},
  mounted() {},
  methods: {},
};
</script>
 `;

export default {
  test1,
};
