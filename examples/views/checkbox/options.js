/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-17 10:49:51
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/table/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Checkbox Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 120
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'value',
        explain: '勾选框选中数据',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'data',
        explain: '勾选框',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'cols',
        explain: '列数',
        type: 'Number',
        validate: '-',
        default: '-'
      },
      {
        params: 'placeholder',
        explain: 'placeholder',
        type: 'String',
        validate: '-',
        default: '请输入搜索内容'
      },
      {
        params: 'allCheckJustFilter',
        explain: '全选时是否只全选筛选',
        type: 'Boolean',
        validate: '-',
        default: true
      }
    ]
  },
  {
    title: 'Checkbox Methods',
    columns: [
      {
        key: 'funtionName',
        title: '方法名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      }
    ],
    data: [
      {
        funtionName: 'clickAllChecked',
        explain: '全选/全反选',
        params: '-'
      }
    ]
  },
  {
    title: 'Checkbox Events',
    columns: [
      {
        key: 'eventName',
        title: '事件名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '回调参数',
        minWidth: 200
      }
    ],
    data: [
      {
        eventName: 'input',
        explain: '选中数据修改',
        params: '当前选中数据'
      }
    ]
  }
];
