/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-17 10:49:51
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/table/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'TypeInput Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'title',
        explain: '标题',
        type: 'Array',
        validate: '-',
        default: '插入动态词包：'
      },
      {
        params: 'types',
        explain: '可选词包',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'value',
        explain: '输入框值',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'chooseTypes',
        explain: '抛出当前选中的词包',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'wordData',
        explain: '待定--垃圾杭没做',
        type: 'Array',
        validate: '-',
        default: '-'
      }
    ]
  },
  {
    title: 'TypeInput Events',
    columns: [
      {
        key: 'eventName',
        title: '事件名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '回调参数',
        minWidth: 200
      }
    ],
    data: [
      {
        eventName: 'input',
        explain: '输入框值修改',
        params: '输入框值'
      }
    ]
  }
];
