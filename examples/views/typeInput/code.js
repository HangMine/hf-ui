/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 13:01:39
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/typeInput/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
    <h-type-input v-model="value" :types="types" :wordData="wordData"> </h-type-input>
</template>

<script>
export default {
  data() {
    return {
      code,
      value: '',
      types: [
        { id: 4, name: '地点' },
        { id: 1727, name: '日期' },
        { id: 1736, name: '星期' },
        { id: 1737, name: '年龄' }
      ],
      wordData: [
        {
          name: '词包1',
          default_word: '默认词1',
          words: [
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1',
            '替换词1'
          ],
          user_rate: '用户覆盖率1',
          status: '状态1'
        }
      ]
    };
  }
};
</script>
 `;

export default {
  test1
};
