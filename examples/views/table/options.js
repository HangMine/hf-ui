/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 12:44:56
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/table/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Table Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'url',
        explain: '表格数据接口（也可通过getTable属性传入函数，见下面参数）',
        type: '{columns:[],rows:[],footer:{}}',
        validate: '-',
        default: '-'
      },
      {
        params: 'getTable',
        explain: '传入一个函数(参数为接口参数，返回{columns:[],data:[]})',
        type: 'params=>Promise.resolve({columns:[],rows:[],footer:{}})',
        validate: '-',
        default: '-'
      },
      {
        params: 'type',
        explain: '分页类型',
        type: '前端分页或者后端分页，默认后端分页（前端写死数据不需传Url或者getTable，但需要指定type为front）',
        validate: 'back/front',
        default: 'back'
      },
      {
        params: 'columns',
        explain: '表头数据',
        type: 'Array',
        validate: '-',
        default: '接口获取'
      },
      {
        params: 'data',
        explain: '表格数据',
        type: 'Array',
        validate: '-',
        default: '接口获取'
      },
      {
        params: 'footer',
        explain: '汇总数据',
        type: 'Object',
        validate: '-',
        default: '接口获取'
      },
      {
        params: 'params',
        explain: '异步请求会进行合并的参数，优先级最高',
        type: 'Object',
        validate: '-',
        default: '-'
      },
      {
        params: 'form',
        explain: '表单数据（具体结构参考h-form）',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'maxHeight',
        explain: '最大高度',
        type: 'String|Number',
        validate: '-',
        default: '到屏幕最下面'
      },
      {
        params: 'border',
        explain: '是否有分隔线',
        type: 'Boolean',
        validate: '-',
        default: 'true'
      },
      {
        params: 'size',
        explain: '表格大小',
        type: 'String',
        validate: '	medium | small | mini',
        default: 'small'
      },
      {
        params: 'align',
        explain: '单元格的text-align',
        type: 'String',
        validate: '	center | left | right',
        default: 'center'
      },
      {
        params: 'tool',
        explain: '操作列参数',
        type: 'Object',
        validate: '{minWidth:Number}',
        default: 'small'
      },
      {
        params: 'operate',
        explain: '新增编辑参数',
        type: 'Object',
        validate: '{form:Array(表单数据),submitUrl:String(新增编辑接口)}',
        default: 'small'
      },
      {
        params: 'page',
        explain: '分页相关参数,可传入elelment pageOptions 的所有参数，或者传入false去除分页器',
        type: 'Object | Boolean',
        validate: '-',
        default: `{
          currentSize: 10,
          pageSizes: [10, 20, 30, 40, 50],
          layout: 'total, sizes, prev, pager, next, jumper'
        }`
      },
      {
        params: '_export',
        explain:
          '导出相关参数，传true或者一个对象开启导出功能，对象结构：{url:"导出地址",name:"导出名字",text:"按钮文字"}',
        type: 'Object | Boolean',
        validate: '-',
        default: 'false'
      }
    ]
  },
  {
    title: 'Column Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'key',
        explain: '表头的key值',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'title',
        explain: '表头的显示值',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'sort',
        explain: '表头的排序类型（前端排序需要指定类型，后端排序指定falsy值即可）',
        type: 'Boolean | String',
        validate: 'string/int/percent/date/week',
        default: '-'
      },
      {
        params: 'children',
        explain: '合并表头的子表头',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'addWidth',
        explain: '在计算后的宽度上增加的宽度（可以是负值）',
        type: 'Number',
        validate: '-',
        default: '-'
      },
      {
        params: '...',
        explain: '其它所有elemenut table column所支持的值',
        type: 'Any',
        validate: '-',
        default: '-'
      }
    ]
  },
  {
    title: 'Table Events',
    columns: [
      {
        key: 'eventName',
        title: '事件名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '回调参数',
        minWidth: 200
      }
    ],
    data: [
      {
        eventName: 'add',
        explain: '自定义时的新增事件,不传operate时生效',
        params: 'row,index'
      },
      {
        eventName: 'edit',
        explain: '自定义时的编辑事件,不传operate时生效',
        params: 'row,index'
      },
      {
        eventName: 'del',
        explain: '自定义时的删除事件',
        params: 'row,index'
      }
    ]
  },
  {
    title: 'Table Methods',
    columns: [
      {
        key: 'fn',
        title: '方法名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '参数',
        minWidth: 200
      }
    ],
    data: [
      {
        fn: 'load',
        explain: '加载表格,返回一个promoise',
        params: '-'
      }
    ]
  },
  {
    title: 'Table Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: '表头key值',
        explain: '自定义单元格内容'
      },
      {
        name: 'tool',
        explain: '操作列单元格内容'
      },
      {
        name: 'tool-btn',
        explain: '操作列编辑删除后面的按钮'
      },
      {
        name: 'form',
        explain: '筛选框表单内容'
      },
      {
        name: 'search-btn',
        explain: '搜索按钮右边的位置'
      },
      {
        name: 'btns',
        explain: '按钮行'
      },
      {
        name: 'btn',
        explain: '新增后面的按钮'
      },
      {
        name: 'edit-modal',
        explain: '新增编辑模态框内容'
      }
    ]
  }
];
