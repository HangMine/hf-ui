/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 12:39:50
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/table/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
    // 获取接口数据：使用url或者getTable返回一个Promise的方式，接口返回数据：{data:{columns:[],rows:[]}}
    <h-table url="/getTable" :getTable="getTable"> </h-table>
</template>

<script>

  const getTable = (params) => {
    return axios({
      url: "/getTable",
      params,
    }).then(({ data }) => data);
  };

  export default {
    data() {
      return {
        getTable
      };
    }
  };
</script>
 `;

const test1_1 = `
<template>
    // 获取接口数据：使用url或者getTable返回一个Promise的方式，接口返回数据：{data:{columns:[],rows:[]}}
    <h-table type="front" url="/getTable" :getTable="getTable"> </h-table>
</template>

<script>

  const getTable = (params) => {
    return axios({
      url: "/getTable",
      params,
    }).then(({ data }) => data);
  };

  export default {
    data() {
      return {
        getTable
      };
    }
  };
</script>
 `;

const test2 = `
<template>
    <h-table :getTable="getTable" :form="form"> </h-table>
</template>

<script>

  const getTable = (params) => {
    return axios({
      url: "/getTable",
      params,
    }).then(({ data }) => data);
  };

  export default {
    data() {
      return {
        getTable,
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

const test2_1 = `
<template>
    // 后端分页：优先使用_export指定的url，传:export="true"的话，默认是使用url（最终导出地址是url+筛选参数+action:export）
    <h-table :url="/tableUrl" :_export="{url:'/tableUrl'}" :form="form"> </h-table>
</template>

<script>

  export default {
    data() {
      return {
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

const test2_2 = `
<template>
    // 通过export属性设置为true或对象（比如指定名字{name:"导出名字"}）
    <h-table
      type="front"
      :url="tableUrl"
      :_export="{ name: '前端导出文件' }"
      :getTable="getTable"
      :form="form"
      :max-height="false"
    >
    </h-table>
</template>

<script>

  export default {
    data() {
      return {
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

const test2_3 = `
<template>
    // column.sort不为falsy值时开启排序功能
    <h-table :url="tableUrl"></h-table>
</template>

<script>

  export default {
    data() {
      return {
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

const test2_4 = `
<template>
    // column.sort不为falsy值时开启排序功能，前端需指定排序类型，支持类型参考参数说明
    <h-table type="front" :url="tableUrl"></h-table>
</template>

<script>

  export default {
    data() {
      return {
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

const test3 = `
<template>
    <h-table :getTable="getTable" :form="form" :editProps="{ form, submitUrl: '/local/operate' }" @del="del"> </h-table>
</template>

<script>

  const getTable = (params) => {
    return axios({
      url: "/getTable",
      params,
    }).then(({ data }) => data);
  };

  export default {
    data() {
      return {
        getTable,
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

const test4 = `
<template>
  <h-table
  :getTable="getTable"
  :form="form"
  :operate="{ form, submitUrl: '/local/operate', delUrl: '/local/delete' }"
  :max-height="false"
  @del="del"
  >
    <template #name="{row}">
      <ul>
        <li v-for="(item, i) of row.name" :key="i">{{ item }}</li>
      </ul>
    </template>
  </h-table>
</template>

<script>

  const getTable = (params) => {
    return axios({
      url: "/getTable",
      params,
    }).then(({ data }) => data);
  };

  export default {
    data() {
      return {
        getTable,
        form: [
          {
            key: "1",
            title: "测试1",
          },
          {
            key: "2",
            title: "测试2",
          },
          {
            key: "3",
            title: "测试3",
            type: "date",
          },
        ],
      };
    }
  };
</script>
 `;

export default {
  test1,
  test1_1,
  test2,
  test2_1,
  test2_2,
  test2_3,
  test2_4,
  test3,
  test4
};
