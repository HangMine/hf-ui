/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 21:33:13
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/fix/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Fix Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'target',
        explain: '容器',
        type: 'HTMLElement',
        validate: '-',
        default: '-'
      },
      {
        params: 'top',
        explain: '偏移量',
        type: 'Number',
        validate: '-',
        default: 0
      }
    ]
  },
  {
    title: 'Fix Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: 'default',
        explain: '需要固定的元素'
      }
    ]
  }
];
