/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-17 20:46:08
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/fix/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
  <div class="d-fix-test" ref="test">
    <h-fix :target="scrollEl">
      <el-button type="primary">固定在容器的顶部</el-button>
    </h-fix>
    <div class="test1"></div>

    <div class="test2"></div>
  </div>
</template>

<script>
export default {
  data() {
    return {
      scrollEl: null,
    };
  },
  mounted() {
    this.scrollEl = this.$refs.test;
  },
};
</script>
 `;

const test2 = `
<template>
  <div class="d-fix-test" ref="test2">

    <div class="test1"></div>

    <h-fix :target="scrollEl2" :top="20">
      <el-button type="primary">固定在离这个容器20px处</el-button>
    </h-fix>

    <div class="test2"></div>
  </div>
</template>

<script>
export default {
  data() {
    return {
      scrollEl2: null,
    };
  },
  mounted() {
    this.scrollEl2 = this.$refs.test2;
  },
};
</script>
 `;

export default {
  test1,
  test2
};
