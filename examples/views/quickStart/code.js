/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 15:29:48
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/quickStart/code.js
 * @Description: 暂无描述
 */

const allLoad = `
import Vue from 'vue';
import HfUI from 'hf-ui';
import 'hf-ui/index/index.css';
import App from './App.vue';

Vue.use(HfUI);

new Vue({
  el: '#app',
  render: h => h(App)
});
 `;

const installImportPlugin = `
# 安装插件
npm i -D babel-plugin-import

// 在 babel.config.js 中配置
module.exports = {
  plugins: [
    [
      'import',
      {
        libraryName: 'hf-ui',
        style: name => \`\${name}/index.css\`,
        camel2DashComponentName: false
      }
    ]
  ]
};
// 接着你可以在代码中直接引入 hf-ui 组件
// 插件会自动将代码转化为方式二中的按需引入形式
import { HTable } from 'hf-ui';
 `;

export default {
  allLoad,
  installImportPlugin
};
