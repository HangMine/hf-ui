/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 13:11:22
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/cascader/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Cascader Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'data',
        explain: `多级级联的渲染数据，格式为：
        [{
          key:'',
          title:'',
          children:[{
            key:'',
            title:''
          }]
        }]`,
        type: 'Array',
        validate: '-',
        default: '[]'
      },
      {
        params: 'value',
        explain: 'v-model的值，多选为[[],[]]的格式，单选为[]',
        type: 'Array',
        validate: '-',
        default: '[]'
      },
      {
        params: 'radio',
        explain: '开启单选模式（默认为多选）',
        type: 'Boolean',
        validate: '-',
        default: 'false'
      }
    ]
  }
];
