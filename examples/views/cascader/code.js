/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-26 14:03:43
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/cascader/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
  <h-cascader v-model="cascader" :data="cascaderData"></h-cascader>
</template>

<script>
export default {
  data() {
    cascaderData: [
      {
        key: '1',
        title: '测试'
      },
      {
        key: '2',
        title: '测试2',
        children: [
          {
            key: '33',
            title: '33'
          },
          {
            key: '88',
            title: '88'
          }
        ]
      },
      {
        key: '3',
        title: '测试3'
      },
      {
        key: '4',
        title: '测试4',
        children: [
          {
            key: '5',
            title: '测试5'
          },
          {
            key: '6',
            title: '测试6',
            children: [
              {
                key: '22',
                title: '22'
              },
              {
                key: '33',
                title: '33'
              }
            ]
          },
          {
            key: '7',
            title: '测试7'
          }
        ]
      }
    ],
    cascader: []
  },
};
</script>
 `;

const test1_1 = `
<template>
  <h-cascader v-model="cascader" :data="cascaderData"></h-cascader>
</template>

<script>
export default {
  data() {
    cascaderData: [
      {
        key: '1',
        title: '测试'
      },
      {
        key: '2',
        title: '测试2',
        children: [
          {
            key: '33',
            title: '33'
          },
          {
            key: '88',
            title: '88'
          }
        ]
      },
      {
        key: '3',
        title: '测试3'
      },
      {
        key: '4',
        title: '测试4',
        children: [
          {
            key: '5',
            title: '测试5'
          },
          {
            key: '6',
            title: '测试6',
            children: [
              {
                key: '22',
                title: '22'
              },
              {
                key: '33',
                title: '33'
              }
            ]
          },
          {
            key: '7',
            title: '测试7'
          }
        ]
      }
    ],
    cascader: [['2'], ['4-5'], ['4-6-22']]
  },
};
</script>
 `;

const test2 = `
<template>
  <h-cascader v-model="cascader" :data="cascaderData" radio></h-cascader>
</template>

<script>
export default {
  data() {
    cascaderData: [
      {
        key: '1',
        title: '测试'
      },
      {
        key: '2',
        title: '测试2',
        children: [
          {
            key: '33',
            title: '33'
          },
          {
            key: '88',
            title: '88'
          }
        ]
      },
      {
        key: '3',
        title: '测试3'
      },
      {
        key: '4',
        title: '测试4',
        children: [
          {
            key: '5',
            title: '测试5'
          },
          {
            key: '6',
            title: '测试6',
            children: [
              {
                key: '22',
                title: '22'
              },
              {
                key: '33',
                title: '33'
              }
            ]
          },
          {
            key: '7',
            title: '测试7'
          }
        ]
      }
    ],
    cascader: []
  },

};
</script>
 `;

const test2_1 = `
<template>
  <h-cascader v-model="cascader" :data="cascaderData"></h-cascader>
</template>

<script>
export default {
  data() {
    cascaderData: [
      {
        key: '1',
        title: '测试'
      },
      {
        key: '2',
        title: '测试2',
        children: [
          {
            key: '33',
            title: '33'
          },
          {
            key: '88',
            title: '88'
          }
        ]
      },
      {
        key: '3',
        title: '测试3'
      },
      {
        key: '4',
        title: '测试4',
        children: [
          {
            key: '5',
            title: '测试5'
          },
          {
            key: '6',
            title: '测试6',
            children: [
              {
                key: '22',
                title: '22'
              },
              {
                key: '33',
                title: '33'
              }
            ]
          },
          {
            key: '7',
            title: '测试7'
          }
        ]
      }
    ],
    cascader: ['2', '2-77']
  },
};
</script>
 `;

export default {
  test1,
  test1_1,
  test2,
  test2_1
};
