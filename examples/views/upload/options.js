/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 22:42:44
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/upload/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Upload Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'url',
        explain: '上传URL',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'chunkSize',
        explain: '切片大小',
        type: 'Number',
        validate: '-',
        default: '0.05'
      },
      {
        params: 'handleParams',
        explain: 'parents的处理函数，直接修改params或返回一个新的params',
        type: 'Function',
        validate: '-',
        default: '-'
      },
      {
        params: 'maxSize',
        explain: '文件最大限制',
        type: 'Number',
        validate: '-',
        default: '-'
      },
      {
        params: 'accept',
        explain: '文件类型',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'tip',
        explain: '提示',
        type: 'String',
        validate: '-',
        default: '请先选择文件（可批量），再点击上传'
      }
    ]
  },
  {
    title: 'Upload Events',
    columns: [
      {
        key: 'eventName',
        title: '事件名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '回调参数',
        minWidth: 200
      }
    ],
    data: [
      {
        eventName: 'process',
        explain: '获取上传进度',
        params: '{percent, file, files}'
      },
      {
        eventName: 'success',
        explain: '上传成功',
        params: '{file, files}'
      },
      {
        eventName: 'fail',
        explain: '上传失败',
        params: '{file, files}'
      },
      {
        eventName: 'error',
        explain: '文件上传异常',
        params: '{file, files}'
      }
    ]
  }
];
