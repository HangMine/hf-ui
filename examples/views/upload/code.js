/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 12:12:40
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/upload/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
    <h-upload
      url="/local/upload"
      :handleParams="handleParams"
      @process="onProcess"
      @success="onSuccess"
      @fail="onFail"
      @error="onError"
    ></h-upload>
</template>

<script>
export default {
  methods: {
    handleParams(params) {
      // 可以直接修改params
      params.file2 = params.fileName;
      // 可以自定定一个新的对象
      // return { file2: params.fileName };
    },
    onProcess({ percent,file, files }) {
      console.log(file, files);
    },
    onSuccess({ file, files }) {
      console.log(file, files);
    },
    onFail({ file, files }) {
      console.log(file, files);
    },
    onError({ file, files }) {
      console.log(file, files);
    }
  }
};
</script>
 `;

export default {
  test1
};
