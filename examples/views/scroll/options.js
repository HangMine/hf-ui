/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 21:58:55
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/scroll/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Scroll Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'to',
        explain: '跳转位置，px',
        type: 'Number, String',
        validate: '-',
        default: '-'
      },
      {
        params: 'el',
        explain: '跳转位置，DOM元素的selector',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'transition',
        explain: '跳转过渡效果秒数',
        type: 'Number',
        validate: '-',
        default: '200'
      },
      {
        params: 'height',
        explain: '容器高度，默认继承父元素高度',
        type: 'Number, String',
        validate: '-',
        default: '100%'
      }
    ]
  },
  {
    title: 'Scroll Methods',
    columns: [
      {
        key: 'fn',
        title: '方法名',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'params',
        title: '参数',
        minWidth: 200
      }
    ],
    data: [
      {
        fn: 'scrollTo',
        explain: '滚动到目标点',
        params: 'target(scrollTop | selector)'
      }
    ]
  },
  {
    title: 'Scroll Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: 'default',
        explain: '需要滚动的元素'
      }
    ]
  }
];
