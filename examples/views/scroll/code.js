/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-17 21:29:09
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/scroll/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
    <h-scroll :height="500" :to="scrollTop" :el="scrollEl" @scroll="onScroll" @scroll-end="scrollEnd">
      <h-card>
        <el-button type="primary" @click="scrollTo(200)">跳转到200px</el-button>
        <el-button type="primary" @click="scrollTo('.d-scroll-test')">跳转到回到顶部按钮</el-button>
      </h-card>
      <div v-for="n of 9" :key="n" class="d-scroll-test1">
        <p>这是</p>
        <p>很长的</p>
        <p>文字</p>
        <p>~</p>
        <p>~</p>
        <p>~</p>
      </div>
      <el-button type="primary" class="d-scroll-test" @click="scrollTo(0)">回到顶部</el-button>
    </h-scroll>
</template>

<script>
export default {
  data() {
    return {
      code,
      scrollTop: undefined,
      scrollEl: '',
    };
  },
  methods: {
    scrollTo(target) {
      if (typeof target === 'number') {
        this.scrollTop = target;
      } else if (typeof target === 'string') {
        this.scrollEl = target;
      }
    },
    onScroll(scrollTop) {
      // console.log(scrollTop);
    },
    scrollEnd() {
      // 使得可以重复触发滚动事件
      this.scrollTop = undefined;
      this.scrollEl = '';
    }
  }
};
</script>
`;

export default {
  test1
};
