/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 10:54:06
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/export/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
    <h-export url="/getTable?action=export"></h-export>
</template>
 `;

const test2 = `
<template>
    <h-export :data="exportData" name="前端导出测试"></h-export>
</template>

<script>
export default {
  data() {
    return {
      exportData: "表头1,表头2,表头3\\n1,2,3,\\n",
    };
  }
};
</script>
 `;

const test3 = `
<template>
    <h-export :get-export-data="getExportData" name="getExportData导出测试"></h-export>
</template>

<script>
export default {
  methods:{
    getExportData(exportComponent){
      return '表头1,表头2,表头3\n1,2,3,\n'
    }
  }
};
</script>
 `;
export default {
  test1,
  test2,
  test3
};
