/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 10:59:22
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/export/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Export Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 100
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 200
      }
    ],
    data: [
      {
        params: 'url',
        explain: '后端导出地址',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'name',
        explain: '前端端导出文件名字（不用后缀）',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'data',
        explain: '前端导出数据（csv格式，具体看例子）',
        type: 'String',
        validate: '-',
        default: '-'
      },
      {
        params: 'url',
        explain: '前端通过函数返回导出数据（csv格式，具体看例子）',
        type: '()=>String',
        validate: '-',
        default: '-'
      },
      {
        params: 'text',
        explain: '导出按钮文字',
        type: 'String',
        validate: '-',
        default: '导出'
      }
    ]
  }
];
