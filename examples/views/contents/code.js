/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 13:27:41
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/contents/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
  <h-content>
    <h2 :h-content="test1.title">{{ test1.title }}</h2>
    <p>{{ test1.explain }}</p>
    <h-hight-light :code="test1.code" min-height="auto"></h-hight-light>

    <options-tables :options="options"></options-tables>
  </h-content>
</template>

<script>
import code from './code';
import options from './options';
export default {
  name: 'd-content',
  data() {
    return {
      options,
      test1: {
        title: '基础使用',
        explain:
          '目录组件，由滚动内容+目录组成，需要成为标题的元素设置属性h-content="标题内容"即可。例如该页面便使用了目录组件，代码如下：',
        code: code.test1
      }
    };
  },
};
</script>
 `;

export default {
  test1
};
