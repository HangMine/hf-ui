/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 21:40:14
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/contents/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'Hcontent Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 120
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 200
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'value',
        explain: '勾选框选中数据',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'data',
        explain: '勾选框',
        type: 'Array',
        validate: '-',
        default: '-'
      },
      {
        params: 'cols',
        explain: '列数',
        type: 'Number',
        validate: '-',
        default: '-'
      },
      {
        params: 'placeholder',
        explain: 'placeholder',
        type: 'String',
        validate: '-',
        default: '请输入搜索内容'
      },
      {
        params: 'allCheckJustFilter',
        explain: '全选时是否只全选筛选',
        type: 'Boolean',
        validate: '-',
        default: true
      }
    ]
  },

  {
    title: 'Hcontent Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: 'default',
        explain:
          '需要跟目录同步滚动的元素，需要成为标题的元素设置属性h-content="标题内容"（目录根据标题内容.trim()设置，不能同名）'
      }
    ]
  }
];
