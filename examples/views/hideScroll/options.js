/*
 * @Author: Hang
 * @Date: 2020-07-16 22:17:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-20 21:33:43
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/hideScroll/options.js
 * @Description: 暂无描述
 */

export default [
  {
    title: 'HideScroll Attributes',
    columns: [
      {
        key: 'params',
        title: '参数',
        minWidth: 150
      },
      {
        key: 'explain',
        title: '说明',
        minWidth: 200,
        className: 'ellipsis'
      },
      {
        key: 'type',
        title: '类型',
        minWidth: 100
      },
      {
        key: 'validate',
        title: '可选值',
        minWidth: 100
      },
      {
        key: 'default',
        title: '默认值',
        minWidth: 100
      }
    ],
    data: [
      {
        params: 'maxHeight',
        explain: '表单配置数据',
        type: 'Number, String',
        validate: '-',
        default: '-'
      },
      {
        params: 'unMutationObserver',
        explain: '默认会监听所有子节点的style和class的变化，以设置最新的高度,但会有一定性能的消耗',
        type: 'Boolean',
        validate: '-',
        default: 'false'
      }
    ]
  },
  {
    title: 'HideScroll Slots',
    columns: [
      {
        key: 'name',
        title: 'name',
        minWidth: 100
      },
      {
        key: 'explain',
        title: '说明'
      }
    ],
    data: [
      {
        name: 'default',
        explain: '需要隐藏滚动条的元素'
      }
    ]
  }
];
