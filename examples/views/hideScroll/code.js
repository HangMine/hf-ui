/*
 * @Author: Hang
 * @Date: 2020-07-13 17:26:31
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 22:00:24
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/views/hideScroll/code.js
 * @Description: 暂无描述
 */

const test1 = `
<template>
  <h-hide-scroll :max-height="300">
    <div class="test"></div>
  </h-hide-scroll>
</template>

<style lang="scss">
  .test {
    height: 1000px;
    background: indianred;
  }
</style>
 `;

export default {
  test1,
};
