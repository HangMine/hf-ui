/*
 * @Author: Hang
 * @Date: 2020-07-16 20:13:07
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-16 23:05:58
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/examples/components/index.js
 * @Description: 暂无描述
 */
import ContentTitle from './ContentTitle';
import Demo from './Demo';
import OptionsTables from './OptionsTables';

const components = [ContentTitle, Demo, OptionsTables];

const install = Vue => {
  components.forEach(component => Vue.component(component.name, component));
};

export default install;
