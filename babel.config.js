/*
 * @Author: Hang
 * @Date: 2020-07-18 01:27:59
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 15:23:10
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/babel.config.js
 * @Description: 暂无描述
 */

module.exports = {
  presets: [
    [
      '@vue/cli-plugin-babel/preset',
      {
        useBuiltIns: false
      }
    ]
  ],
  plugins: [
    [
      'import',
      {
        libraryName: 'hf-ui',
        style: name => `${name}/index.css`,
        camel2DashComponentName: false
      }
    ]
  ]
};
