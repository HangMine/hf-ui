/*
 * @Author: Hang
 * @Date: 2020-07-13 12:04:38
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-21 21:28:49
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/api/http.js
 * @Description: 暂无描述
 */
import Vue from 'vue';
import axios from 'axios';
import qs from 'qs';

const isDev = process.env.NODE_ENV === 'development';
const devBaseUrl = 'local';
const prodBaseUrl = '';

let exampleAxios = axios.create({
  baseURL: isDev ? devBaseUrl : prodBaseUrl
});

// 全局的post请求设置为表单格式
// axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';

exampleAxios.interceptors.request.use(
  config => {
    const { url, method, params = {} } = config;
    let newParams;
    if (method === 'post') {
      // post参数也通过params传递
      config.data = params;
      config.params = '';
      newParams = config.data;
    } else if (method === 'get') {
      newParams = config.params;
    }

    // post请求也使用'application/x-www-form-urlencoded'的传值
    // if (method === 'post') {
    //   config.data = qs.stringify(newParams);
    // }
    return config;
  },
  error => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);
// 为实例添加响应拦截器
exampleAxios.interceptors.response.use(
  response => {
    // 只返回接口数据
    return response;
  },
  error => {
    return Promise.reject(error);
  }
);

export const getUrl = url => (isDev ? devBaseUrl + url : prodBaseUrl + url);

Vue.prototype.$http = exampleAxios;

export default exampleAxios;
