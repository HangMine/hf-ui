/*
 * @Author: Hang
 * @Date: 2020-07-13 14:02:38
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-22 12:10:03
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/api/map.js
 * @Description: 暂无描述
 */

import http from './http';

export const getTable = params => {
  return http({
    url: '/getTable',
    params
  }).then(({ data }) => data);
};
