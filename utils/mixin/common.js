/*
 * @Author: Hang
 * @Date: 2020-07-15 18:54:26
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 20:31:14
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/utils/mixin/common.js
 * @Description: 暂无描述
 */

import { getType } from '@utils/common';
export default {
  methods: {
    formCheck(form) {
      let pass;
      const comp = this.$refs[form];

      if (comp.formCheck) {
        comp.formCheck();
      } else {
        comp.validate(valid => {
          pass = valid;
        });
      }
      return pass;
    },
    clearCheck(form) {
      //必须使用nextTick保证在弹窗出现后执行
      this.$nextTick(() => {
        const comp = this.$refs[form];
        if (comp.clearCheck) {
          comp.clearCheck();
        } else {
          comp.clearValidate();
        }
      });
    },
    getInitObj(originObj = {}, preValueObj = {}) {
      let obj = {};
      for (const [key, value] of Object.entries(originObj)) {
        obj[key] = preValueObj.hasOwnProperty(key) ? preValueObj[key] : getInitValue(value);
      }
      return obj;
    }
  }
};

// 根据类型设置默认值
const getInitValue = value => {
  const type = getType(value);
  switch (type) {
    case 'array':
      return [];
    case 'object':
      return {};
    case 'date':
      return new Date();
    case 'number':
      return 0;
    case 'null':
    case 'undefined':
    case 'function':
      return value;
    default:
      return '';
  }
};
