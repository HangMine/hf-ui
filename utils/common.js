/*
 * @Author: Hang
 * @Date: 2020-07-11 16:35:02
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-18 15:53:16
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/utils/common.js
 * @Description: 通用的工具类
 */

/**
 * @description: 获取目标的类型
 * @param {Any} target
 * @return {String}
 */
export const getType = target => {
  if (target instanceof Element) {
    return 'element';
  }
  let type = Object.prototype.toString.call(target);
  type = type.slice(8, -1).toLowerCase();
  return type;
};

/**
 * @description:
 * @param {Number | String} num
 * @return
 */
export const getPx = num => (getType(num) === 'number' ? `${num}px` : num);

/**
 * @description: 参数兼容对象模式,只适用于装饰器模式（参数有默认值的情况必须传入自定义的形参数组！因为有默认值的情况下获取不到形参的key值）
 * @param {Array | undefined} argKeys
 * @return {Function}
 */
export function argsAdapt(argKeys) {
  // 是否传入形参数组
  const diyArgKeys = Array.isArray(argKeys);
  if (!diyArgKeys) {
    const fn = arguments[2].value;
    argKeys = getFuncArgKeys(fn);
  }

  const mainFn = (target, name, descriptor) => {
    const fn = descriptor.value;

    descriptor.value = function() {
      let argValues = Array.from(arguments);

      const arg1 = arguments[0];
      if (getType(arg1) === 'object') {
        for (const [i, argKey] of Object.entries(argKeys)) {
          const argValue = arg1[argKey];
          if (argValue) argValues[i] = argValue;
        }
      }

      fn(...argValues);
    };
  };

  if (diyArgKeys) {
    return mainFn;
  } else {
    mainFn(...arguments);
  }
}

/**
 * 获取函数的形参个数
 * @param  {Function} func 要获取的函数
 * @return {Array} 形参的数组
 */
const getFuncArgKeys = func => {
  let args = [];
  if (typeof func === 'function') {
    const mathes = /[^(]+\(([^)]*)?\)/gm.exec(Function.prototype.toString.call(func));
    if (mathes[1]) {
      args = mathes[1].replace(/[^,\w]*/g, '').split(',');
    }
  }
  return args;
};

/**
 * @des: 深复制(es6，不添加原型版本)，需考虑循环引用对象的兼容处理:https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
 * @param {Any} data 需要复制的数据
 * @param {Array} cache 保存已经遍历过的对象，是为了处理循环引用对象
 * @return {Any} 复制后的数据
 */
export const deepCopy = (obj, cache = []) => {
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }
  const hit = cache.filter(c => c.original === obj)[0];
  if (hit) {
    return hit.copy;
  }
  const copy = Array.isArray(obj) ? [] : {};
  cache.push({
    original: obj,
    copy
  });
  Object.keys(obj).forEach(key => {
    copy[key] = deepCopy(obj[key], cache);
  });
  return copy;
};

/**
 * @description: 节流（默认立即执行）
 * @param {Function} fn 执行函数
 * @param {Number} cycle 执行周期
 * @return {Function}
 */
export const throttle = (fn, cycle = 100) => {
  let timer;
  return function() {
    const execFn = () => {
      fn.apply(this, arguments);
      timer = null;
    };
    // timer === undefined 时，为首次注册
    if (timer === undefined) {
      execFn();
    } else if (timer === null) {
      timer = setTimeout(() => {
        execFn();
      }, cycle);
    }
  };
};

/**
 * @description: 防抖
 * @param {Function} fn 执行函数
 * @param {Number} cycle 延迟时间
 * @return {Function}
 */
export const debounce = (fn, delay = 100) => {
  let timer;
  return function() {
    const execFn = () => {
      fn.apply(this, arguments);
    };
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      execFn();
    }, delay);
  };
};

/**
 * @description: 数组去重(每次只跟当前值右边的值进行比较)
 * @param {Array} array
 * @return {Array}
 */
export const getUniqArray = (array, fn) => {
  var temp = [];
  var index = [];
  var l = array.length;
  for (var i = 0; i < l; i++) {
    for (var j = i + 1; j < l; j++) {
      let v1 = typeof fn === 'function' ? fn(array[i]) : array[i];
      let v2 = typeof fn === 'function' ? fn(array[j]) : array[j];
      if (v1 === v2) {
        // 如果有相同值，则跳过，先遍历下一个值
        i++;
        j = i;
      }
    }
    // 没有重复值的才会推入temp
    temp.push(array[i]);
    index.push(i);
  }
  return temp;
};

/**
 * @description: 首字母大写
 * @param {String} str
 * @return {String}
 */
export const toCapital = str => {
  if (typeof str !== 'string') return;
  return str.charAt(0).toUpperCase() + str.slice(1);
};

/**
 * @description: 转驼峰
 * @param {String} str
 * @return {String}
 */
export const camelCase = str => str.replace(/-(.)/g, (_, w) => w.toUpperCase());

/**
 * @description: 复制内容到剪贴版
 * @param {String} val 需要复制的内容
 * @return:
 */
export const copy = val => {
  //使用textarea才可以换行
  let textarea = document.createElement('textarea');
  textarea.value = val;
  document.body.appendChild(textarea);
  textarea.select();
  document.execCommand('Copy');
  textarea.parentNode.removeChild(textarea);
};

/**
 * @description: 生成唯一标识
 * @return {String}
 */
export const uuid = () => {
  var s = [];
  var hexDigits = '0123456789abcdef';
  for (var i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
  }
  s[14] = '4'; // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = '-';

  var uuid = s.join('');
  return uuid;
};

/**
 * @description: 获取字符串长度，中文算2字节
 * @param {String} str
 * @return {String}
 */
export function getLength(str = '') {
  let length = 0;
  for (const singleStr of str) {
    const isChinese = /[\u4e00-\u9fa5]/.test(singleStr);
    const singleLength = isChinese ? 2 : 1;
    length += singleLength;
  }
  return length;
}
