/*
 * @Author: Hang
 * @Date: 2020-07-12 17:41:39
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-12 18:14:02
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/utils/hoc.js
 * @Description: Vue高阶组件相关
 */

export const compose = (...funcs) => (...args) =>
  funcs.reduce((a, b) => a(b(...args)));

// 透传属性，vue 2.6 $scopedSlots包含了$slots和$scopedSlots
export const extendBase = (vm) => ({
  attrs: vm.$attrs,
  on: vm.$listeners,
  scopedSlots: vm.$scopedSlots,
});

/**
 * @description: 透传属性后返回原函数
 * @param {Function} fn
 * @return {Function}
 */
export const hoc = (fn) =>
  compose(
    (Wrapped) => ({
      render: (h) => <Wrapped></Wrapped>,
    }),
    fn
  );

export const WithPromise = (promiseFn) => {
  return (wrappedComponent) => {
    return {
      name: "with-promise",
      data() {
        return {
          loading: true,
          error: false,
          result: null,
        };
      },
      async mounted() {
        const result = await promiseFn().finally(() => {
          this.loading = false;
        });
        this.result = result;
        // 这时候才有refs
        console.log(this.$refs);
      },
      render() {
        return this.loading ? (
          <div>加载中...</div>
        ) : this.error ? (
          <div>加载错误</div>
        ) : (
          <wrappedComponent
            {...extendBase(this)}
            ref="wrappedComponent"
          ></wrappedComponent>
        );
      },
    };
  };
};

export const WithConsole = (wrappedComponent) => {
  return {
    mounted() {
      console.log("WithConsole print");
    },
    render() {
      return <wrappedComponent {...extendBase(this)}></wrappedComponent>;
    },
  };
};
