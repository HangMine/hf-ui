/*
 * @Author: Hang
 * @Date: 2020-07-14 16:00:12
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 00:30:00
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/utils/work.js
 * @Description: 含有部分依赖的公用函数，比如依赖弹出消息、axios等
 */
import axios from 'axios';
import { Message } from 'element-ui';
import { copy as _copy, uuid } from './common';

/**
 * @description: 复制内容到剪贴版
 * @param {String} val 需要复制的内容
 * @param {String} msg 复制成功提示
 * @return:
 */
export const copy = (val, msg = '复制成功') => {
  _copy(val);
  if (Message) {
    Message({
      message: msg,
      type: 'success',
      showClose: true
    });
  }
};

/**
 * @description: 切片上传
 * @param {String} url 文件上传地址
 * @param {Object} params 额外参数
 * @param {ArrayBuffer} file 文件
 * @param {Number} size 切片大小(单位为M)
 * @param {Number} chunk 从第几片开始上传
 * @param {Function} onProcess 进度回调
 * @param {Function} onSuccess 成功回调
 * @param {Function} onFail 失败回调
 * @param {Function} onError 错误回调
 * @return:
 */
export function uploadChunk({
  url,
  handleParams,
  file,
  size = 1,
  chunk = 0, //从第几片开始上传
  onProcess,
  onSuccess,
  onFail,
  onError
}) {
  size = size * 1024 * 1024;
  const chunks = Math.ceil(file.size / size); //总片数
  uploadSlice();

  function uploadSlice() {
    const start = size * chunk;
    const end = size * (chunk + 1);
    const blobSlice = file.mozSlice || file.webkitSlice || file.slice;
    const uid = uuid(); //增加唯一标识
    const slice = blobSlice.call(file, start, end);

    let formParams = {
      file: slice,
      fileName: file.name,
      chunk,
      chunks,
      uid
    };
    if (typeof handleParams === 'function') {
      const newParams = handleParams(formParams);
      if (newParams) formParams = newParams;
    }
    let formdata = new FormData();
    Object.entries(formParams).forEach(([key, val]) => formdata.append(key, val));
    chunk++; //chunk在这里加一
    axios(url, {
      method: 'post',
      data: formdata,
      onUploadProgress: e => {
        const progress = size * chunk + e.loaded;
        let percent = Math.floor((progress / file.size) * 100);
        if (percent > 100) percent = 100; //当文件不够切片大小的时候，会超出100，在这里兼容
        onProcess && onProcess(percent);
      }
    })
      .then(res => {
        //后续修改成返回成功标识时
        if (res.data.code == 0) {
          //最后一个时给出成功回调
          chunk < chunks ? uploadSlice() : onSuccess && onSuccess(res);
        } else {
          //失败回调，chunk回退1
          onFail && onFail(res, chunk - 1);
        }
      })
      .catch(err => {
        //错误回调，chunk回退1
        onError && onError(err, chunk - 1);
      });
  }
}
