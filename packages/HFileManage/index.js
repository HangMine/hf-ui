/*
 * @Author: Hang
 * @Date: 2020-07-14 22:16:34
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-14 22:16:50
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HFileManage/index.js
 * @Description: 暂无描述
 */
import HFileManage from './HFileManage.vue';
export default {
  install(Vue) {
    Vue.component(HFileManage.name, HFileManage);
  }
};
