/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 18:00:02
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HModal/index.js
 * @Description: 暂无描述
 */

import HModal from './HModal.vue';
export default {
  install(Vue) {
    Vue.component(HModal.name, HModal);
  }
};
