/*
 * @Author: Hang
 * @Date: 2020-07-13 19:29:26
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 10:41:36
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HExport/index.js
 * @Description: 暂无描述
 */

import HExport from './HExport.vue';
export default {
  install(Vue) {
    Vue.component(HExport.name, HExport);
  }
};
