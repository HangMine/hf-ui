/*
 * @Author: Hang
 * @Date: 2020-07-11 19:59:44
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 10:41:03
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/index.js
 * @Description: 组件引入入口
 */
import '@assets/css/index.scss';

import Ok from './Ok';
import HForm from './HForm';
import HTable from './HTable';
import HHightLight from './HHightLight';
import HTimeRange from './HTimeRange';
import HCheckboxGroup from './HCheckboxGroup';
import HHideScroll from './HHideScroll';
import HCard from './HCard';
import HDivider from './HDivider';
import HUpload from './HUpload';
import HFileManage from './HFileManage';
import HOverHide from './HOverHide';
import HTypeInput from './HTypeInput';
import HModal from './HModal';
import HScroll from './HScroll';
import HCatalog from './HCatalog';
import HContent from './HContent';
import HFix from './HFix';
import HCascader from './HCascader';
import HExport from './HExport';

const components = [
  Ok,
  HForm,
  HTable,
  HHightLight,
  HTimeRange,
  HCheckboxGroup,
  HHideScroll,
  HCard,
  HDivider,
  HUpload,
  HFileManage,
  HOverHide,
  HTypeInput,
  HModal,
  HScroll,
  HCatalog,
  HContent,
  HFix,
  HCascader,
  HExport
];

const install = Vue => {
  components.forEach(comp => Vue.use(comp));
};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

// 需要每个导出，后续考虑是否可以使用webpack的require.context
export default {
  install, // 全量引入
  Ok,
  HForm,
  HTable,
  HHightLight,
  HTimeRange,
  HCheckboxGroup,
  HHideScroll,
  HCard,
  HDivider,
  HUpload,
  HFileManage,
  HOverHide,
  HTypeInput,
  HModal,
  HScroll,
  HCatalog,
  HContent,
  HFix,
  HCascader,
  HExport
};
