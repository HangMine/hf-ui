/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-12 17:18:43
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTable/index.js
 * @Description: 暂无描述
 */

import HTable from "./HTable.vue";
export default {
  install(Vue) {
    Vue.component(HTable.name, HTable);
  },
};
