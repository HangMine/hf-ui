/*
 * @Author: Hang
 * @Date: 2020-07-15 19:27:37
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 20:14:32
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTable/mixin/editModal.js
 * @Description: 暂无描述
 */

export default {
  props: {
    editProps: {
      type: Object,
      default: () => ({})
    }
  },
  data() {
    return {
      isEdit: false,
      editShow: false,
      editParams: {},
      editFormValues: {}
    };
  },
  computed: {
    editTitle() {
      return this.isEdit ? '编辑' : '新增';
    }
  }
};
