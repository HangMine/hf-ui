/*
 * @Author: Hang
 * @Date: 2020-07-15 14:41:06
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 17:46:01
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTable/mixin/cellClick.js
 * @Description:
 * 使用: table添加事件监听@cell-dblclick="cellClick"，名字必须是"cellClick"
 */

// 使用该mixin
import { copy } from '@utils/work';
export default {
  mounted() {
    this.headerFooterClick();
  },
  desdroyed() {
    this.$refs.table.$el.removeEventListener('dbclick', this.handleHeaderFooterClick);
  },
  methods: {
    //监听单元格数据
    cellClick(row, column, cell, event) {
      if (event.target.tagName == 'I') return; //如果点击图标则不复制
      let cellText = row[column.property].name || row[column.property];
      copy(cellText);
    },
    //监听汇总数据
    headerFooterClick() {
      this.$refs.table && this.$refs.table.$el.addEventListener('dblclick', this.handleHeaderFooterClick);
    },
    handleHeaderFooterClick(e) {
      let dom = e.target.nodeType == 3 ? e.target.parentNode : e.target;
      if (dom.tagName == 'SPAN') dom = dom.parentNode;
      if (dom.parentNode.classList.contains('is-leaf')) {
        let cellText = dom.textContent;
        copy(cellText);
      }
    }
  }
};
