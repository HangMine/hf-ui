/*
 * @Author: Hang
 * @Date: 2020-07-22 17:27:43
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-22 21:13:51
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTable/mixin/export.js
 * @Description: 暂无描述
 */
import qs from 'qs';
export default {
  props: {
    _export: {
      /* 
        对象格式为：{
          url: '',
          text: '',
          ...export组件支持的属性
        }
      */
      type: [Boolean, Object],
      default: false
    }
  },
  data() {
    return {
      exportStr: ''
    };
  },
  computed: {
    exportUrl() {
      if (this.type === 'front') return '';
      const url = (this._export && this._export.url) || this.url || '';
      const backExportUrl = `${url}?${qs.stringify({ ...this.formParams, ...this.params, action: 'export' })}`;
      return backExportUrl;
    }
  },
  methods: {
    getExportData() {
      // 普通表头数组
      let keys = [];
      // 固定表头数组
      let fixKeys = [];
      const getHeader = (columns = this.currentColumns) => {
        let titles = [];
        let fixTitles = [];

        const helper = (arr, prevItems = []) => {
          for (let item of arr) {
            if (item.children && item.children.length) {
              helper(item.children, [...prevItems, item]);
            } else {
              const title = [...prevItems, item].map(item => item.title).join('-');
              let targetTitles = item.fixed ? fixTitles : titles;
              let targetKeys = item.fixed ? fixKeys : keys;
              targetTitles.push(title);
              targetKeys.push(item.key);
            }
          }
        };

        helper(columns);

        let titlesStr = [...fixTitles, ...titles].join() + '\n';
        return titlesStr;
      };
      const getRows = (rows = this.initRows) => {
        let rowsArr = [];
        for (let item of rows) {
          let rowArr = [];
          for (let key of [...fixKeys, ...keys]) {
            let val = item[key] == '0' ? '0' : item[key] || ''; //兼容0的判断
            let objVal = val && val.title && val.title == '0' ? '0' : (val && val.title) || ''; //兼容0的判断
            let resVal = objVal || val;
            if (+resVal && resVal.length > 11) resVal = `\t${resVal}`; //超过11位的数字不显示计数法
            resVal = `${resVal}`.replace(/,/g, '-'); //','是csv格式，需修改为'-'

            rowArr.push(resVal);
          }
          rowsArr.push(rowArr.join() + '\n');
        }
        let rowsStr = rowsArr.join('');
        return rowsStr;
      };
      const getFooter = (footer = this.footer) => {
        let footerArr = Object.values(footer);
        let footerStr = footerArr ? footerArr.join() + '\n' : '';
        return footerStr;
      };

      return getHeader() + getRows() + getFooter();
    }
  }
};
