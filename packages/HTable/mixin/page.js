/*
 * @Author: Hang
 * @Date: 2020-07-22 14:28:57
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-24 11:26:29
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTable/mixin/page.js
 * @Description: 暂无描述
 */

// 只是保存数据，不需要双向绑定那套重逻辑

// 前端分页需要保存初始数据，从里面切割
export let initRows = [];
// 前端分页需要保存初始数据，重新计算表头宽度
export let initColumns = [];

export default {
  props: {
    type: {
      type: String,
      default: 'back'
    },
    // 分页相关参数,详情参照element 的分页组件,可通过false取消分页
    page: {
      type: [Object, Boolean],
      default: () => ({})
    }
  },
  data() {
    return {
      // 分页
      pageOptions: {
        total: 0,
        currentPage: 1,
        currentSize: 10,
        pageSizes: [10, 20, 30, 40, 50],
        layout: 'total, sizes, prev, pager, next, jumper'
      }
    };
  },
  computed: {
    // 分页参数
    pageParams() {
      const backPageParams = {
        page: this.pageOptions.currentPage,
        size: this.pageOptions.currentSize
      };
      return this.type === 'back' && this.page ? backPageParams : {};
    }
  },
  watch: {
    page: {
      handler(val) {
        this.pageOptions = { ...this.pageOptions, ...val };
      },
      immediate: true
    }
  },
  methods: {
    // 分页事件
    sizeChange(size) {
      this.pageOptions.currentSize = size;
      // elementUI会处理成最后一页，触发pageChange
      if (size * this.pageOptions.currentPage > this.pageOptions.total) return;
      this.pageMain();
    },
    pageChange(page) {
      this.pageOptions.currentPage = page;
      this.pageMain();
    },
    // 分页主逻辑函数
    pageMain() {
      this.type === 'back' ? this.backPageMain() : this.frontPageMain();
    },
    // 后端分页主逻辑
    backPageMain() {
      this.load();
    },
    // 前端分页主逻辑
    frontPageMain(_initRows = initRows) {
      this.currentRows = this.getSlicesRows(_initRows);
      this.currentColumns = this.handleColumns(initColumns);
    },
    // 获取前端分页的数据
    getFrontPageRows(rows, columns) {
      this.setInitData(rows, columns);
      return this.getSlicesRows(rows);
    },
    // 设置前端分页需要保存的初始数据
    setInitData(rows, columns) {
      initRows = rows;
      initColumns = columns;
    },
    // 获取切割后的数据
    getSlicesRows(_initRows = initRows) {
      if (!this.page) return _initRows;
      const page = this.pageOptions.currentPage;
      const size = this.pageOptions.currentSize;
      const start = (page - 1) * size;
      const end = page * size;
      return _initRows.slice(start, end);
    }
  }
};
