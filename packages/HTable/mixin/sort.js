import { initRows } from './page';
export default {
  data() {
    return {
      sortType: '', //string | int | percent | date | week
      sortKey: '', //列
      sortOrder: '' //ascending/descending
    };
  },
  computed: {
    sortParams() {
      if (this.type === 'front' || !this.sortOrder) return {};
      return {
        sortKey: this.sortKey,
        sortOrder: this.sortOrder
      };
    }
  },
  methods: {
    // 排序
    sortChange(column) {
      this.sortKey = column.prop;
      this.sortOrder = column.order;
      if (!column.order) {
        //不排序时回复原来排序
        this.resetRows();
        return;
      }
      this.type === 'back' ? this.backSort() : this.frontSort();
    },
    // 前端排序
    frontSort() {
      let header = this.getHeader(this.sortKey);
      if (!header) return; //兼容切换表头时找不到数据的报错
      this.sortType = header.sort;
      const sortFn = getSortFn(this.sortType);
      // 正常排序
      let _initRows = [...initRows];
      sortFn(_initRows, this.sortKey, this.sortOrder);
      this.frontPageMain(_initRows);
    },
    // 后端排序
    backSort() {
      this.load ? this.load() : this.$parent.load();
    },

    getHeader(key) {
      const columns = this.currentColumns.flat();
      const column = columns.find(item => item.key === key);
      return column;
    },
    resetRows() {
      // 调用 page mixin的函数
      this.pageMain();
    }
  }
};

const getSortFn = type => {
  switch (type) {
    case 'string':
      return sort_string;
    case 'int':
      return sort_int;
    case 'percent':
      return sort_percent;
    case 'date':
      return sort_date;
    case 'week':
      return sort_week;
    default:
      break;
  }
};

const sort_string = (currentRows, key, order) => {
  currentRows.sort((a, b) => {
    let v1 = (a[key] && a[key].name && `${a[key].name}`) || `${a[key]}` || '';
    let v2 = (b[key] && b[key].name && `${b[key].name}`) || `${b[key]}` || '';
    let asc = v1.localeCompare(v2, 'zh-Hans-CN', {
      sensitivity: 'accent'
    });
    let desc = v2.localeCompare(v1, 'zh-Hans-CN', {
      sensitivity: 'accent'
    });
    return order == 'ascending' ? asc : desc;
  });
};
const sort_int = (currentRows, key, order) => {
  currentRows.sort((a, b) => {
    let v1 = `${(a[key] && a[key].name) || a[key]}`;
    let v2 = `${(b[key] && b[key].name) || b[key]}`;
    v1 = +v1.replace('--', 0).replace(/\(.+\)/, '');
    v2 = +v2.replace('--', 0).replace(/\(.+\)/, '');
    return order == 'ascending' ? v1 - v2 : v2 - v1;
  });
};
const sort_percent = (currentRows, key, order) => {
  currentRows.sort((a, b) => {
    let v1 = `${(a[key] && a[key].name) || a[key]}`;
    let v2 = `${(b[key] && b[key].name) || b[key]}`;
    v1 = +v1
      .replace('%', '')
      .replace('--', 0)
      .replace(/\(.+\)/, '');
    v2 = +v2
      .replace('%', '')
      .replace('--', 0)
      .replace(/\(.+\)/, '');
    return order == 'ascending' ? v1 - v2 : v2 - v1;
  });
};

const sort_date = (currentRows, key, order) => {
  currentRows.sort((a, b) => {
    let v1 = getDate(a, key);
    let v2 = getDate(b, key);
    return order == 'ascending' ? v1 - v2 : v2 - v1;
  });
};
const sort_week = (currentRows, key, order) => {
  currentRows.sort((a, b) => {
    let v1 = getWeek(a, key);
    let v2 = getWeek(b, key);
    return order == 'ascending' ? v1 - v2 : v2 - v1;
  });
};

const getNumber = item => {
  const value = `${(item && item.name) || item}`;
  return +value.replace(/[^\d.]/g, '');
};
const getDate = (val, key) => {
  val = (val[key] && val[key].name) || val[key];
  if (isNaN(val)) {
    if (val == '--') return new Date(0); //兼容'--'的情况
    val = val.replace(/-/g, '/');
    return new Date(val);
  } else {
    return +val;
  }
};

const getWeek = (val, key) => {
  val = (val[key] && val[key].name) || val[key];
  val = val.replace(/[\u4e00-\u9fa5]/g, '').replace(/-/g, ''); //去除中文和-
  return +val;
};
