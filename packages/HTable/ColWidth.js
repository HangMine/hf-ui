/*
 * @Author: Hang
 * @Date: 2020-07-15 14:41:06
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-23 09:53:52
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTable/ColWidth.js
 * @Description:
 * 对每个表头设置wid属性
 * 每个column可传入:
 * minWidth:最小宽度
 * width:固定宽度
 * addWidth:在计算的宽度上加减的宽度
 */
import { deepCopy, getType } from '@utils/common';

// 意外的值宽度
const LAST_WIDTH = 100;
// 每列最大宽度
const COL_MAX_WIDTH = 400;
// 单字符宽度
const SIGNLE_WIDTH = 8;
// 双字符宽度
const DOUBLE_WIDTH = 13;
// 排序图标宽度;
const SORT_ICON_WIDTH = 10;
// elementUI表格单元格padding: 10+10(需要看table的size)
const CEIL_PADDING_WIDTH = 20;
// 当宽度超出最大宽度时，增加该class
const WIDTH_MORE_THAN_MAX = 'width-more-than-max';

class ColWidth {
  constructor(cols = [], rows = [], footer = {}) {
    return this.getWidCols(cols, rows, footer);
  }
  getWidCols(cols = [], rows = [], footer = {}) {
    let columns = deepCopy(cols);

    // 递归兼容多级表头
    const helper = (columns, rows, footer) => {
      for (const item of columns) {
        if (item.children) {
          helper(item.children, rows, footer);
        } else {
          if (item.minWidth || item.width) continue;
          let headerWid = this.getLength(item.title) + SORT_ICON_WIDTH;
          let contentWid = this.getContentMaxWidth(rows, item.key);
          let footerWid = this.getFooterMaxWidth(footer, item.key);
          let addWidth = item.addWidth || 0;
          let calcWid = Math.max(headerWid, contentWid, footerWid) + CEIL_PADDING_WIDTH + addWidth; //addWid为自定义需要要增加的宽度
          if (calcWid > COL_MAX_WIDTH) {
            item.className = WIDTH_MORE_THAN_MAX;
          }
          item.minWidth = Math.min(calcWid, COL_MAX_WIDTH);
        }
      }
    };

    helper(columns, rows, footer);
    return columns;
  }
  getContentMaxWidth(rows, col) {
    if (!rows || !rows.length) {
      return 0;
    }
    let lengthArr = rows.map(item => this.getLength(item[col]));
    let maxLength = Math.max(...lengthArr);
    return maxLength;
  }
  getFooterMaxWidth(footer, col) {
    return footer && Object.keys(footer).length ? this.getLength(footer[col]) : 0;
  }

  getLength(val) {
    switch (getType(val)) {
      case 'number':
        return Math.round(`${val}`.length * SIGNLE_WIDTH);
      case 'string':
        return this.handleString(val);
      case 'object':
        if (!val || val.name == 0) return 0; //值为null或者'0'时
        if (val.name) {
          return this.handleString(`${val.name}`);
        }
        return LAST_WIDTH;
      case 'array':
        return this.getArrayLength(val);
      default:
        return val ? LAST_WIDTH : 0; //固定宽度100，值为falsy则为0
    }
  }
  // 获取数组长度，在这里递归
  getArrayLength(arr = []) {
    const lengths = arr.map(item => this.getLength(item));
    return Math.max(...lengths);
  }
  handleString(val) {
    let doubleLength = (val.match(/[^\x00-\xff]/g) && val.match(/[^\x00-\xff]/g).length) || 0;
    let singleLength = val.length - doubleLength;
    return doubleLength * DOUBLE_WIDTH + Math.round(singleLength * SIGNLE_WIDTH);
  }
}

export default ColWidth;
