/*
 * @Author: Hang
 * @Date: 2020-07-13 22:56:22
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 22:56:31
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HCard/index.js
 * @Description: 暂无描述
 */
import HCard from "./HCard.vue";
export default {
  install(Vue) {
    Vue.component(HCard.name, HCard);
  },
};
