/*
 * @Author: Hang
 * @Date: 2020-07-14 10:12:41
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-14 10:22:52
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HDivider/index.js
 * @Description: 暂无描述
 */

import HDivider from "./HDivider.vue";
export default {
  install(Vue) {
    Vue.component(HDivider.name, HDivider);
  },
};
