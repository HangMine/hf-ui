/*
 * @Author: Hang
 * @Date: 2020-07-13 18:30:28
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 18:31:48
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTimeRange/index.js
 * @Description: 暂无描述
 */

import HTimeRange from "./HTimeRange.vue";
export default {
  install(Vue) {
    Vue.component(HTimeRange.name, HTimeRange);
  },
};
