/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-16 13:31:00
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HContent/index.js
 * @Description: 暂无描述
 */

import HContent from './HContent.vue';
export default {
  install(Vue) {
    Vue.component(HContent.name, HContent);
  }
};
