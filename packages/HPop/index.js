/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 18:22:18
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HPop/index.js
 * @Description: 暂无描述
 */

import HPop from "./HPop.vue";
export default {
  install(Vue) {
    Vue.component(HPop.name, HPop);
  },
};
