/*
 * @Author: Hang
 * @Date: 2020-07-13 18:30:28
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 09:57:07
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HTypeInput/index.js
 * @Description: 暂无描述
 */

import HTypeInput from './HTypeInput.vue';
export default {
  install(Vue) {
    Vue.component(HTypeInput.name, HTypeInput);
  }
};
