/*
 * @Author: Hang
 * @Date: 2020-07-13 21:29:57
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 21:30:10
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HHideScroll/index.js
 * @Description: 暂无描述
 */
import HHideScroll from "./HHideScroll.vue";
export default {
  install(Vue) {
    Vue.component(HHideScroll.name, HHideScroll);
  },
};
