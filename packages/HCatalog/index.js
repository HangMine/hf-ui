/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-16 12:39:17
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HCatalog/index.js
 * @Description: 暂无描述
 */

import HCatalog from './HCatalog.vue';
export default {
  install(Vue) {
    Vue.component(HCatalog.name, HCatalog);
  }
};
