/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 19:19:56
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HCheckbox/index.js
 * @Description: 暂无描述
 */

import HCheckboxGroup from "./HCheckboxGroup.vue";
export default {
  install(Vue) {
    Vue.component(HCheckboxGroup.name, HCheckboxGroup);
  },
};
