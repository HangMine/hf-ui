/*
 * @Author: Hang
 * @Date: 2020-07-13 18:30:28
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-14 15:55:15
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HUpload/index.js
 * @Description: 暂无描述
 */

import HUpload from "./HUpload.vue";
export default {
  install(Vue) {
    Vue.component(HUpload.name, HUpload);
  },
};
