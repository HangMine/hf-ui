/*
 * @Author: Hang
 * @Date: 2020-07-13 19:29:26
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 19:29:55
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HEmpty/index.js
 * @Description: 暂无描述
 */

import HEmpty from "./HEmpty.vue";
export default {
  install(Vue) {
    Vue.component(HEmpty.name, HEmpty);
  },
};
