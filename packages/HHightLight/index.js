/*
 * @Author: Hang
 * @Date: 2020-07-13 16:43:24
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-13 16:43:57
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HHightLight/index.js
 * @Description: 暂无描述
 */
import HHightLight from "./HHightLight.vue";
export default {
  install(Vue) {
    Vue.component(HHightLight.name, HHightLight);
  },
};
