/*
 * @Author: Hang
 * @Date: 2020-07-13 18:30:28
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 22:18:02
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HScroll/index.js
 * @Description: 暂无描述
 */

import HScroll from './HScroll.vue';
export default {
  install(Vue) {
    Vue.component(HScroll.name, HScroll);
  }
};
