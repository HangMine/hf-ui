/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-15 10:05:37
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HOverHide/index.js
 * @Description: 暂无描述
 */

import HOverHide from './HOverHide.vue';
export default {
  install(Vue) {
    Vue.component(HOverHide.name, HOverHide);
  }
};
