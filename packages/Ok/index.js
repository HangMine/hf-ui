/*
 * @Author: Hang
 * @Date: 2020-07-11 16:24:29
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-21 15:23:03
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/Ok/index.js
 * @Description: 成功浮窗提示
 */

import Vue from 'vue';
import OkComponent from './Ok.vue';
// import { argsAdapt } from "@utils/common";
const OkConstructor = Vue.extend(OkComponent);

class Ok {
  constructor() {}
  install(Vue) {
    Vue.prototype.$ok = new Ok();
  }
  // @argsAdapt(["msg", "delay"])
  show(msg, delay = 3000) {
    const vm = new OkConstructor({ el: document.createElement('div') });
    vm.open(msg);
    document.body.appendChild(vm.$el);
    setTimeout(() => {
      vm.close();
    }, delay);
    return vm;
  }
}

export default new Ok();
