/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-26 11:34:41
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HCascader/index.js
 * @Description: 暂无描述
 */

import HCascader from './index.vue';
export default {
  install(Vue) {
    Vue.component(HCascader.name, HCascader);
  }
};
