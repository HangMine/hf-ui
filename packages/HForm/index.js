/*
 * @Author: Hang
 * @Date: 2020-07-12 14:38:40
 * @LastEditors: Hang
 * @LastEditTime: 2020-07-12 14:49:41
 * @FilePath: /ln-project/Users/zhengmukang/Desktop/hf-ui/packages/HForm/index.js
 * @Description: 暂无描述
 */

import HForm from "./HForm.vue";
export default {
  install(Vue) {
    Vue.component(HForm.name, HForm);
  },
};
